% Using - Template for ICASSP-2017 paper.
% To be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------

\documentclass[letterpaper]{article}
\usepackage{spconf,amsmath,cite}
\usepackage{graphicx}
\usepackage{color}
\usepackage{url,times,enumerate,enumitem,float}
\usepackage{multirow,bigstrut,rotating,booktabs,subcaption}
%\usepackage[font=normal]{subfig}
\usepackage{longtable,ltcaption}
\DeclareCaptionJustification{nohyphen}{\hyphenpenalty=100}
\captionsetup{justification=nohyphen}
\hyphenpenalty=800
\widowpenalty=500
\clubpenalty=500
\setlength{\emergencystretch}{2pt}
\usepackage{grfext}
\PrependGraphicsExtensions*{.pdf,.PDF}
\allowdisplaybreaks
\usepackage{etoolbox}
\apptocmd{\thebibliography}{\ninept}{}{}
\urlstyle{same}



% Title.
% ------
%\title{Indirect Evaluation of Source Separation Measures for Singing Voice Separation through Various Music information Retrieval Tasks}
\title{Evaluation of Source Separation Measures in Context of Singing Voice Separation for Pitch and Melody Extraction}


% Title.
% ------
%\title{AUTHOR GUIDELINES FOR ICASSP 2017 PROCEEDINGS MANUSCRIPTS}
%
% Single address.
% ---------------
%\name{Author(s) Name(s)\thanks{Thanks to XYZ agency for funding.}}
%\address{Author Affiliation(s)}
%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
\name{Udit Gupta$^{\star}$ \qquad
	  Alexander Lerch$^{\dagger}$ \qquad
	  Elliot Moore $^{\star}$
	 } 
\address{$^{\star}$School of Electrical and Comp. Engineering,
		 $^{\dagger}$Center for Music Technology\\
		 Georgia Institute of Technology, Atlanta, GA, USA\\
		 \{uditgupta, alexander.lerch, em80\}@gatech.edu
		}
%

% --------------
%\threeauthors
% {Udit Gupta} {Sch. of Electrical and Comp. Eng.\\Georgia Inst. of Technology\\ {\tt uditgupta@gatech.edu}}
% {Elliot Moore II} {Sch. of Electrical and Comp. Eng.\\Georgia Inst. of Technology\\ {\tt em80@gatech.edu}}
% {Alexander Lerch} {Center for Music Tech.\\Georgia Inst. of Technology\\ {\tt alexander.lerch@gatech.edu}}
%

\begin{document}
\begin{sloppy}
%\ninept

%
\maketitle
%
\begin{abstract} 
Singing Voice Separation (SVS) uses audio source separation methods to isolate the vocal component
from the background accompaniment in music mixes and is often used as a preprocessing step for other information retrieval tasks such as pitch or melody extraction.
The choice of SVS algorithm can have a huge impact on the performance of the subsequent information retrieval task and since it is not practical to exhaustively evaluate SVS algorithms, informed decision must be made based on reported evaluation results of SVS algorithms.
The current state-of-the-art objective measures used to evaluate SVS performance have been developed as noise level ratios for interference, artifacts, and other distortions.
This paper aims to investigate the usefulness the current state-of-the-art objective measures used to evaluate SVS for the purpose of predicting their performance in the context of subsequent use for pitch and melody extraction tasks.
The investigation analyzes the sensitivity of pitch and melody tracking algorithms across different performance criteria for the degradation in performance with different levels of interference present, and then tests the predictive relevance of the objective SVS evaluation measures by comparing the SVS evaluation results to the drop in performance for the pitch or melody tracking tasks.

\end{abstract}
%

\begin{keywords}
	Source Separation, Music Information Retrieval
\end{keywords}

\vspace{-1em}
\section{Introduction}\label{sec:introduction}

Singing Voice Separation (SVS) has gained prominence as a Music Information Retrieval (MIR)
task in the recent years; the goal is to separate the lead vocals from the accompaniment in mixes of (popular) music.
Various algorithms for music information retrieval tasks such as melody extraction\cite{Durrieu4517573,hsu2010singing,yeh2012hybrid}, automatic lyrics recognition\cite{wang2003automatic}, singer identification\cite{fujihara2005singer,mesaros2007singer}, query by singing/humming\cite{Yu2008}, etc., use SVS or partial source separation techniques as preprocessing steps in the process.
Since an exhaustive test of different SVS algorithms for impact on the performance in these use cases is unrealistic, the choice must be made based on the reported performance of the SVS algorithms.
This paper aims to test the reliability of the state-of-the-art objective evaluation measures used for SVS for predicting their impact on the performance of two MIR tasks, pitch and melody extraction.

Studies have been performed in the past where the impact of source separation techniques was studied on performance in tasks such as speech recognition \cite{DiPersia20071951,DiPersia20082578} and instrument recognition \cite{bosch2012comparison}.
In the study by Persia et~al. \cite{DiPersia20082578}, an evaluation of these measures was presented in context of
use of source separation for preprocessing audio for speech recognition tasks. It was concluded from this study that measures based on perceptual evaluation of speech quality (PESQ) show substantial correlation with speech recognition performance.
However, at this time the authors could not find any sources which addressed the challenges stated earlier.

In recent Music Information Retrieval Evaluation eXchange (MIREX) competitions \cite{svsTask14,svsTask15,svsTask16}, the performance of various submissions for the SVS task has been tested and the quality of the output produced by these algorithms was evaluated and reported using  Normalized Signal to Distortion Ratio (NSDR), Signal to Interference Ratio (SIR) and Signal to Artifacts Ratio (SAR)\cite{Emiya2011,Emiya2010}.
Although measures such as SIR, SAR and (N)SDR have been tested for consistency with human perception \cite{Emiya2011,gupta2015perceptual}, their reliability for predicting performance for subsequent use in other MIR tasks is relatively unknown.

%Objective evaluation of Singing Voice Separation systems has traditionally been done through general
%source separation measures which are designed to be consistent with subjective perceptual evaluation.
%Emiya et al.\cite{Emiya2011} have suggested objective measures based on the presence of 
%target spatial distortion (Image to Spatial Distortion Ratio, ISR), 
%interference (SIR), and artifacts (SAR) in the separated signals as compared to the clean source signals.
%The total distortion in the output signal compared to the source is measured by Signal to Distortion Ratio (SDR)\cite{Araki2012}.
%These are used as the current state of the art for evaluation of SVS systems and were used to evaluate the submissions
%in the MIREX SVS tasks, along with the normalized version of SDR designated as NSDR\cite{svsTask14}.
%This paper aims to investigate the quality of these source separation measures SIR, SAR, and NSDR
%when they are used for the purpose of evaluating the SVS task and determine
%how well the results hold when the separated vocals are used for pitch or melody extraction.

This investigation is based on the premise that MIR tasks like pitch and melody extraction will have an upper-bound on their performance when unmixed clean vocals are used as the input to the system and it is expected that the performance will degrade if interfering sources such as instrumental accompaniment are also present. 
Pitch or melody extraction tasks may also be susceptible to distortion and other types of degradation in audio content introduced by the SVS algorithms which may or may not be perceivable to human listeners due to auditory masking effects.

In this paper we first investigate the susceptibility of pitch/melody extraction algorithms to the presence of
different levels of interference.
The pitch accuracy measures which are observed to be statistically affected by the presence of interference are then used
to evaluate the performance of the state-of-the-art source separation measures SIR, SAR and NSDR.

%\section{Related Work}\label{sec:relatedwork}
%
%Studies have been performed in the past where the impact of source separation techniques was studied on performance in tasks such as speech recognition \cite{DiPersia20071951,DiPersia20082578} and instrument recognition \cite{bosch2012comparison}.
%In the study by Persia et~al. \cite{DiPersia20082578}, an evaluation of these measures was presented in context of
%use of source separation for preprocessing audio for speech recognition tasks. It was concluded from this study that measures based on perceptual evaluation of speech quality (PESQ) show substantial correlation with speech recognition performance.


\vspace{-1em}
\section{Investigation Setup}\label{sec:expdesign}

For the purpose of this investigation the vocal component of four different SVS systems is processed through two monophonic pitch estimation algorithms, RAPT \cite{talkin1995robust} and YAAPT \cite{zahorian2008spectral},
and two polyphonic melody extraction algorithms by Salamon and G{\'o}mez \cite{salamon2012melody}
and Arora and Behera\cite{arora2013line} (further referred to as MELSAL and MELARO, respectively).
The objective assessment of performance for both the pitch extraction task and the polyphonic melody extraction task is performed using criteria such as  voicing recall rate, voicing false alarm rate, raw pitch/chroma accuracies in the voiced region, and the overall pitch/chroma accuracy \cite{melex14}.
The four SVS algorithms that were used to generate the set of inputs for the pitch/melody extraction algorithms are Huang et al.\ \cite{huang2012singing}, Jeong and Lee \cite{Jeong2014}, Rao et al.\ \cite{Rao2014}, and Rafii and Pardo \cite{Rafii2013}.

The procedure used for this investigation consists of two parts.
First, the pitch/melody extraction algorithms are tested for susceptibility to performance degradation due to the presence of interfering sources.
This process is described in \secref{sec:sensitivity} of the paper.
Next, the source separated vocals are processed through the pitch/melody extraction algorithms and the performance across the criteria which are observed to be susceptible to interference are compared with the SIR, SAR and NSDR estimates of these signals as described in \secref{sec:evaluation}.


%The data used and the test bench preparation is described in section~\ref{ssec:dataset}.

\vspace{-1em}
\subsection{Test Data}\label{ssec:dataset}
	For the preparation of the testing set, twenty songs from the Medley DB dataset\cite{bittner2014medleydb} were chosen from the ``pop" and ``singer/songwriter" genres such that they had both vocal and instrumental tracks, had no cross-talk (bleed) between the tracks, and had at least one of each: a vocal melody track, an instrumental melody track, and a drum track. Of the selected twenty songs, six have female singers and the remaining fourteen feature male singers.
	
	The tracks from these songs were combined to form two wave files, the first of which contained only the vocal component (CLV) and second with only the accompaniment (CLA).
	They were both normalized such that they had the same average Zwicker Loudness (ISO 532B) \cite{moore1996revision}. 
	The vocal and instrumental tracks were then mixed to obtain three different mixed tracks such that the ratio of loudness of vocals to accompaniment was +5 dB (P5V), 0 dB (MIX), and -5 dB (M5V).
	All of the mixed tracks were also normalized to same average loudness.
	These tracks all contain different amounts of interference as compared to the lead vocals and were used to determine the sensitivity of the pitch/melody extraction algorithms to the differences in accompaniment levels.
	
%	The mixed track with equal loudness for the vocals and accompaniment (MIX) was used as the input for the four chosen SVS algorithms.
%	All the excerpts were processed with the original implementation of these algorithms, resulting in the
%	audio clips containing the estimated vocals and accompaniment with default parameters as provided by the authors.
%		
\section{Algorithm Sensitivity Testing}\label{sec:sensitivity}


\input{tables/pvalues} % label{tab:pvalues}

In order to determine how the quality of separation of the SVS outputs will affect the performance of the pitch/melody extraction systems being used to process the results, it is essential to test the sensitivity of these algorithms to the presence of interference with audio samples of known levels of accompaniment mixed in.
Assuming that the upper-bound performance is achieved by the pitch or melody tracking algorithms
when the input is an audio sample with clean unmixed vocals (CLV), the CLV signals are processed by the above-mentioned algorithms in order to produce a best case result.
The results obtained from this process are used as the target reference against which all other results are compared in further analysis.
The artificially generated mixes at equal loudness of vocals and instrumentals (MIX) along with plus or minus five decibel vocals (M5V, P5V) are processed through the pitch or melody extraction algorithms and the voicing recall and false alarm rates as well as the overall and raw pitch/chroma accuracies are computed \cite{melex14}.

To evaluate how the performance varies with the different levels of interference in the samples, one-tailed paired Kolmogorov-Smirnov tests are performed on the pairwise combinations of five and ten decibel level differences for the six performance criteria as shown in \tabref{tab:pvalues}. 
The objective performance criterion for which the null hypothesis (there is no statistically significant change in performance with the change in interference levels) is not rejected at a ninety-five percent significance level are not considered for the evaluation of SVS results.

The results show that overall and raw pitch and chroma accuracies increase with the level of difference between the vocals and the interfering accompaniment. This increase in the accuracies is statistically significant as demonstrated in \tabref{tab:pvalues}.
The increase in voicing recall rate, while significant for the two polyphonic melody extraction algorithms (MELSAL and MELARO), is not statistically significant for the monophonic pitch extraction algorithms (RAPT and YAAPT).
The false alarm rates for voicing detection decrease with increase in relative levels of the vocal component in the case of MELSAL and in the case of MELARO the difference is not significant for five decibel increase but statistically significant for a ten ten decibel increase.

The analysis in this section shows that raw and overall pitch and chroma accuracies show statistically significant differences for presence of different levels of interference for all four algorithms, RAPT, YAAPT, MELARO and MELSAL; therefore performance in terms of raw and overall pitch and chroma accuracies should be affected by the amount of residual interference present in the extracted vocals.
However voicing recall rate and the voicing false alarm rate show significant differences only for the polyphonic melody tracking algorithms MELSAL and MELARO, and therefore will only be used for comparison in these two cases.

%It can be concluded from the discussion that raw and overall pitch and chroma accuracies of RAPT and YAAPT are sensitive to the presence of different levels of interference in the form of accompaniment.
%This evidence supports our assumption that the performance of these algorithms for these four criteria will increase with the quality of vocal separation for the same mix being separated using different SVS algorithms.
%Similarly, for polyphonic melody extraction algorithms MELSAL and MELARO, the performance with respect to raw and overall pitch and chroma accuracies, as well as voicing detection and false alarm rates can be expected to improve with increase in the quality of vocal separation.  {\color{red}{I would rephrase this whole paragraph. Just say that as expected we can see that the interference level directly impact the performance of the algorithms, because this is what is of interest as conclusion. Then point out the individual metrics that are and aren't useful. Could you just gray out the metrics you don't use in Table 1?}}

\vspace{-1.5em}
\section{Evaluation of Source Separation Measures}\label{sec:evaluation}
To evaluate the performance of the SVS measures, all the excerpts with equal levels of vocals and accompaniment (MIX) were processed through the SVS algorithms and the source separation measures NSDR, SIR and SAR were calculated for the estimated vocals.
These measures are calculated by using BSS Eval Toolbox (version 3.0) \cite{vincent2012signal} as specified by the evaluation procedures described by MIREX SVS Task specification. SDR, SIR, and SAR were calculated with the unmixed vocals as the target signal and vocal estimates from different SVS algorithms as estimated signals. Additionally, NSDR was calculated by subtracting the SDR obtained earlier from a new estimate of SDR where the target and estimated signals were normalized with respect to the original mix.

SIR, SAR, and NSDR are computed for the outputs of the four SVS algorithms (from the same source) and are compared with the
pitch, chroma, and voicing accuracy performance criteria selected above. 

For each of the comparisons, the objective SVS measures as well as the performance metrics of the pitch/melody tracking algorithms are normalized such that the mean and standard deviation of each quantity is zero and one respectively.
The Pearson's Product Moment Correlation Coefficients between each of the SVS measures and the selected pitch/melody extraction performance criteria  are found and reported.
Additionally, the root mean squared errors (RMSE) and coefficient of determination ($\mathrm{R^2}$) of a linear regression model, using the SVS measure as the predictor and the pitch/melody extraction performance as response,  are also reported.
Sections \ref{ssec:mono} and \ref{ssec:poly} discuss these comparisons for the monophonic pitch tracking algorithms and the polyphonic melody tracking algorithms, respectively.
	
%Pearson's Product Moment Correlation Coefficient is calculated for each source separation measure versus 
%each pitch or melody extraction performance criterion result.
%This process is repeated for all twenty excerpts that are the part of the test data.
%The mean values and the ninety-five percent confidence intervals are calculated for the obtained
%correlation coefficient values by using a non-parametric estimate of their probability distribution\cite{wahba1983bayesian}.
%The correlations for which the confidence interval lies entirely in the positive or negative range and does not cross zero can be considered to be statistically significant with $\alpha = 0.05$ as it means that at least ninety-five percent of pairwise correlation results are all positive or all negative.
%
%The same analysis is repeated with Spearman's Rank Correlation Coefficient in place of Pearson's Correlation Coefficient.
%While Pearson's correlation coefficient gives a measure of linear relationship between the two quantities, Spearman's
%correlation coefficient indicates the degree to which there exists a monotonic relationship between the pair.
%The performance of these source separation measures as compared to the pitch or melody extraction measures is discussed in
%Sections \ref{ssec:mono} and \ref{ssec:poly}.
		
\subsection{Evaluation Using Monophonic Pitch Estimation} \label{ssec:mono}   
	The evaluation has been performed using RAPT \cite{talkin1995robust} and YAAPT\cite{zahorian2008spectral} algorithms.
	The RAPT algorithm uses a normalized cross-correlation method to estimate pitch in monophonic audio sample.
	The YAAPT algorithm estimates pitches using the time-frequency information obtained from processing the audio sample and operates in the spectral domain.
	Both algorithms output a pitch vector with uniform time-step size. The portions of the audio which are determined to be unvoiced 	result in zero output.
	The processing of excerpts through  RAPT was performed with a frame size of ten milliseconds
	with a F0 search range of 50~Hz to 500~Hz.
	While for YAAPT implementation a frame length of twenty-five milliseconds with a spacing of ten milliseconds was used with a search range of F0 candidates from 60~Hz to 400~Hz. 
	
	The results of comparing the RAPT performance criteria with SIR, SAR and NSDR are shown in \tabref{tab:tabRAPT} and the similar results for YAAPT algorithm are shown in \tabref{tab:tabYAAPT}.
	In both the tables, the comparison for which the absolute correlation is substantially high are highlighted, which in this case is between Overall Pitch and Chroma Accuracy and SIR only.
	For both these cases with high correlation, it is observed that the comparisons exhibit high RMSE values and low $\mathrm{R^2}$ values, which suggests that while the average correlation between the overall pitch/chroma accuracies and SIR is high, the predictive behavior of SIR is not very good as the deviations from the predicted values are large.
	The small $\mathrm{R^2}$ values suggest a large amount of scatter between the pitch or chroma estimation accuracy and the other source separation measures. This result was verified by observing the pairwise scatter plots for these quantities.
	
	\input{tables/tabRAPT}
	\input{tables/tabYAAPT}
		
\subsection{Evaluation Using Polyphonic Melody Tracking} \label{ssec:poly}

	In the melody tracking task, algorithms are designed to determine the predominant melody present in the input audio in the presence of polyphonic interference.
	These algorithms are supposed to identify the main melody in the region where multiple melodic voices or instruments are present.
	Since these algorithms are designed to have robust performance in the presence of interference from the instrumental accompaniment, it is expected that the performance will be more sensitive to the distortions and artifacts added by the SVS process instead of the actual level of separation.
	
	For the purpose of evaluation, the melody extraction algorithms by Salamon and G{\'o}mez \cite{salamon2012melody} (MELSAL) and Arora and Behara\cite{arora2013line} (MELARO) are used.
	The implementation of MELSAL was parametrized with a block size of 2048 samples and a step size of
	128; the frequency search range was set to 65~Hz to 1760~Hz.
	A frequency search range of 65~Hz to 1110~Hz was used for MELARO.
	These values reflect the default parameters as provided by the original authors.
		
	\tabref{tab:tabSALAMON} shows the results obtained upon comparing the performance measures for the melody tacking task with the source separation measures SIR, SAR and NSDR, and the same is shown for MELARO in \tabref{tab:tabARORA}.
	Similar to the pitch tracking results, the comparisons which have a substantial absolute value of correlation are highlighted in bold typeface.
	While for MELSAL (in \tabref{tab:tabSALAMON}) SAR shows a high correlation of +0.71 with the voicing recall rate and -0.61 for SIR with the voicing false alarm rate, MELARO does not show any strong correlation between the SVS measures and the melody tracking performance criteria.
	
	For the two cases of high correlation values mentioned above, it is observed that the RMSE values are quite high and the corresponding $\mathrm{R^2}$ values are very low.
	Additionally, due to the lack of consensus between the correlation statistics of MELSAL and MELARO there is insufficient evidence regarding utility of SAR and SIR as predictors of voicing recall and voicing false alarm detection rates respectively for melody extraction algorithms in general.

	
	\input{tables/tabSALAMON} %  \label{tab:tabSALAMON}%
	\input{tables/tabARORA} %  \label{tab:tabARORA}%

\section{Conclusion}\label{sec:conclusion}

The purpose of the analysis presented in this paper was to determine if the state-of-the-art objective measures used for evaluation of singing voice separation have predictive relevance in context of pitch and melody tracking with singing voice separation as an intermediate step.
The investigation results show that the pitch and melody tracking algorithms used for testing show significant levels of degradation for different levels of interference present in the audio.

Although, SIR was shown to have strong correlation with overall pitch and chroma accuracies for monophonic pitch tracking algorithms, no conclusive evidence was found correlating any of the objective measures with the performance criteria for the polyphonic algorithms.

It can be concluded from this investigation that when using SVS as an intermediate step for tasks like pitch or melody tracking, the choice of the SVS algorithm to use can not be decided solely based on the reported SIR, SAR and NSDR statistics.
While SIR shown some predictive relevance for overall pitch and chroma accuracies in the case of monophonic pitch tracking, there is a large amount of variance present in the results.
It must be acknowledged that although the amount of evidence presented in this paper is limited due to the availability of source code for pitch/melody tracking algorithms as well as SVS algorithms, the authors are hopeful that this may pave way for future more intensive investigations regarding the subject.



%The analysis showed that the two monophonic pitch tracking algorithms (RAPT and YAAPT) as well as the two polyphonic melody tracking algorithms (MELSAL and MELARO) under test were significantly impacted in performance by the mixing levels of vocals vs./ instrumentals present in the signal.
%While the accuracy rates for pitch and chroma based criteria changed significantly with change in the mixing levels of the vocals, the voicing detection recall and false-alarm rates were insensitive to the relative levels of interference in the case of monophonic algorithms.
%On the other hand all the six performance criteria demonstrated statistically significant levels of degradation in the case of polyphonic algorithms when the level of interference was increased with respect to the vocals.
%Based on this analysis it was expected that Signal to Interference Ratio (SIR), which measures the relative power levels between the vocals and the interference (instrumentals) would be a good predictor of performance.
%
%Upon correlating the pitch tracking measures with the source separation measures, it was observed that while SIR is significantly correlated with the pitch and chroma overall accuracy, the usefulness of SIR as a predictor of performance in this context is not very high as the results exhibited large deviations from the regression curve.
%
%In the case of polyphonic melody tracking, the results for the two algorithms under test did not agree.
%While for MELSAL it was observed that on an average an increase in SIR resulted in fewer false alarms for voicing detection, it was SAR which showed a high positive correlation with the voicing recall performance.
%All the SVS measures failed to produce any significant correlations with the performance criteria for melody extraction in the case of MELSAL algorithm.

%Although the investigation presented in this paper suggests that there may be some positive correlation between the SIR measure for SVS evaluation and the performance of the SVS algorithm in context of monophonic pitch tracking, the results for the use of the state-of-the-art source separation measures in the context of polyphonic melody extraction remains unclear.
%It must be acknowledged that although the amount of evidence presented in this paper is limited due to the availability of source code for pitch/melody tracking as well as SVS systems, the authors are hopeful that this may pave way for future more intensive investigations regarding the subject. {\color{red}{The conclusions are too thin for my taste. They are mostly a summary of observations. You have to be more clear about what we learn from, what we can conclude, and what the contribution of this paper is really. This is a problem throughout the paper --- WHY are we doing these things and what do we learn from the results has to be worked out more clearly.}}

% For bibtex users:
\bibliographystyle{IEEEbib}
\bibliography{SVS_Library}

\end{sloppy}
\end{document}
