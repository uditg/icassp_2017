% -----------------------------------------------
% Template for ISMIR Papers
% 2016 version, based on previous ISMIR templates

% Requirements :
% * 6+1 page length maximum
% * 2MB maximum file size
% * Copyright note must appear in the bottom left corner of first page
% (see conference website for additional details)
% -----------------------------------------------

% Template for ICASSP-2017 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------

\documentclass{article}
\usepackage{spconf,amsmath,mathtools}
\usepackage{graphicx}
\usepackage{color}
\usepackage{url,times,enumerate,enumitem,float}
\urlstyle{same}
\usepackage{multirow,bigstrut,rotating,booktabs,subcaption}
%\usepackage[font=normal]{subfig}
\usepackage{longtable,ltcaption}
\usepackage{etoolbox}
\usepackage{pgfplots}
\AtBeginEnvironment{align}{\setcounter{subeqn}{0}}% Reset subequation number at start of align
\newcounter{subeqn} \renewcommand{\thesubeqn}{\theequation\alph{subeqn}}%
\newcommand{\subeqn}{%
	\refstepcounter{subeqn}% Step subequation number
	\tag{\thesubeqn}% Label equation
}
\DeclareCaptionJustification{nohyphen}{\hyphenpenalty=10000}
\captionsetup{justification=nohyphen}
%\captionsetup{justification=left}
\hyphenpenalty=800
\widowpenalty=10000
\clubpenalty=10000
\setlength{\emergencystretch}{2pt}
\usepackage{grfext}
\PrependGraphicsExtensions*{.pdf,.PDF}
\usepackage{cite}
\allowdisplaybreaks

%\bibliographystyle{unsrt}

% Title.
% ------
\title{VIPS: A New Measure for Assessment of Preservation of Vocal Intelligibility for Singing Voice Separation}

% Note: Please do NOT use \thanks or a \footnote in any of the author markup

% Two addresses
% --------------
%\twoauthors
%  {Udit Gupta, Elliot Moore II}
%	{Sch. of Electrical and Comp. Eng.\\
%	Georgia Inst. of Technology\\
%	\tt \{uditgupta, em80\}@gatech.edu}
%  {Alexander Lerch}
%	{Center for Music Tech.\\
%	Georgia Inst. of Technology\\
%	\tt alexander.lerch@gatech.edu}

\name{Udit Gupta$^{\star}$ \qquad
	  Alexander Lerch$^{\dagger}$ \qquad
	  Elliot Moore$^{\star}$
	 } 
\address{$^{\star}$School of Electrical and Comp. Engineering,
		 $^{\dagger}$Center for Music Technology\\
		 Georgia Institute of Technology, Atlanta, GA, USA\\
		 \{uditgupta, alexander.lerch, em80\}@gatech.edu
		}

%% To make customize author list in Creative Common license, uncomment and customize the next line
%  \def\authorname{First Author, Second Author} 

% Three addresses
% --------------
%\threeauthors
%  {First Author} {Affiliation1 \\ {\tt author1@ismir.edu}}
%  {Second Author} {\bf Retain these fake authors in\\\bf submission to preserve the formatting}
%  {Third Author} {Affiliation3 \\ {\tt author3@ismir.edu}}

%% To make customize author list in Creative Common license, uncomment and customize the next line
%  \def\authorname{First Author, Second Author, Third Author} 


\sloppy % please retain sloppy command for improved formatting

\begin{document}
\ninept
\maketitle
\begin{abstract}
Singing Voice Separation (SVS) uses audio source separation methods to isolate the vocal component from the background accompaniment in a song mix.
A key challenge currently associated with evaluation of SVS is a lack of objective measures which correlate consistently with subjective evaluation.
Additionally, the current state-of-the-art evaluation measures require the use of unmixed vocal and instrumental tracks which are often not available.
This paper proposes a new objective measure, Vocal Intelligibility-Preservation Score (VIPS), which can be computed without a reference track and is intended to improve on the current state-of-the-art for the evaluation of preservation of vocal intelligibility in SVS systems.
New features are designed to capture the effect of specific artifacts which are introduced by SVS systems and these features are combined using a regression model to compute VIPS.
Results from a listening test across several SVS methods show that the VIPS maintains a higher and more consistent correlation with subjective assessments over current state-of-the-art objective measures.

\end{abstract}

\begin{keywords}
	Source Separation, Music Information Retrieval, Perceptual Evaluation
\end{keywords}


\section{Introduction}\label{sec:introduction}
Singing Voice Separation (SVS) has gained prominence as a Music Information Retrieval task in recent years;
the goal is to separate the lead vocals from the accompaniment in mixes of (popular) music \cite{huang2012singing,Ikemiya2014,Jeong2014,rafii2012music,Rafii2013,Rao2014}.
In Music Information Retrieval Evaluation eXchange (MIREX) in recent years \cite{svsTask14,svsTask15,svsTask16}, the performance of various submissions for the SVS task was evaluated using (Normalized) Signal to Distortion Ratio (NSDR),
Signal to Interference Ratio (SIR), and Signal to Artifacts Ratio (SAR) \cite{Vincent2006,Emiya2010}.
However these measures do not correlate well with perceptual results obtained from human evaluation based on listening tests\cite{gupta2015,Emiya2011}.

These measures and other more recent ones \cite{Emiya2011}, need the presence of unmixed vocal and instrumental tracks for evaluation.
Outside of academic datasets, the unmixed tracks are rarely available, limiting the utility of these measures.
This paper is an attempt to address these shortcomings of the current state-of-the-art measures by proposing a 
new objective measure, Vocal Intelligibility-Preservation Score (VIPS), to better evaluate the performance of various SVS implementations with a closer correlation to the human listening experience for preservation of lyric-intelligibility in separated vocals.
The importance of vocal intelligibility has been argued in the past, and it has been stated that the quality of speech is not always proportional to intelligibility.
However it is maintained that vocal intelligibility is perceptually important in itself even if it is not a measure of quality of separation\cite{vincent2006preliminary}.

VIPS has been designed as a ``no-reference'' measure, i.e. it does not need the unmixed vocal and instrumental tracks for the evaluation.
This proposal for a new feature is a part of a larger effort which aims to design perceptually related objective measures specifically designed for evaluation of SVS in terms of isolation of vocals, preservation intelligibility, additive and subtractive distortions, as well as the overall perceptual quality.

%The rest of the paper is organized as follows. First, an overview of existing state of the art for the evaluation of SVS implementations is presented in Sect.~\ref{sec:overview}. This includes but is not limited to NSDR, SIR and SAR. 
%In Sect.~\ref{sec:setup}, a small summary of a listening test from an earlier work is provided.
%The ratings obtained from this listening test are correlated with the existing measures and the results obtained are discussed.
%These ratings are also used further in the paper to justify the validity of the proposed features.
%Section \ref{sec:edge_detection} describes the new features, and provides the results of their evaluation.
%The concluding remarks are addressed in Sect.~\ref{sec:conclusion}.

\section{Related Work}\label{sec:overview}
There are many examples where perceptual evaluation has been performed
for comparing general audio source separation systems~\cite{Joseph2004,Kornycky2008,Yilmaz2004}. 
Many of these methods are geared towards evaluating source separation in speech-only mixtures and do not transfer
elegantly to SVS evaluation where vocals are mixed with instrumental accompaniment.
Additionally, listening tests are time-consuming, have to be carefully planned,
and are usually restricted to a relatively small subset of audio files.
To address these challenges some objective methods for performance evaluation have been suggested.

Emiya et al.\cite{Vincent2006} have suggested objective measures based on the presence of 
target spatial distortion (Image to Spatial Distortion Ratio, ISR), 
interference (SIR), and artifacts (SAR) in the separated signals as compared to the clean source signals.
The total distortion in the output signal compared to the source is measured by Signal to Distortion Ratio (SDR)\cite{Araki2012}.
These measures model the test signal (extracted vocal) as a linear mixture of the target signal (true vocal), interfering signals (true instrumental), and noise (processing artifacts). The contribution of each of these sources is estimated to get the values for SIR, SAR and (N)SDR. These measures are a part of the publicly available BSS Eval Toolbox\cite{Vincent2006} and will be collectively referred to as BSS Eval measures in this paper.
SIR and SAR were used to evaluate the submissions in the MIREX Singing Voice Separation tasks, along with the normalized version of SDR designated as NSDR\cite{svsTask14}. Moderate correlation in the range of 0.3 to 0.7 has been reported for these measures when compared to judgment by human evaluators for general audio source separation tasks\cite{Emiya2011,fox2007modeling}.

Another set of measures have been developed as improved versions of BSS Eval measures.
These measures, referred to as OPS (Overall Perceptual Score), TPS (Target-related Perceptual Score), IPS (Interference-related Perceptual Score), and APS (Artifacts-reated Perceptual Score) are designed by taking the human loudness perception models into account and have been reported to have higher correlation scores as compared to BSS Eval measures \cite{Emiya2011}.
OPS, TPS, IPS and APS are available as parts of PEASS Toolbox\cite{Emiya2011,vincent2012improved} and will be referred to as PEASS measures collectively in this paper.
Both BSS Eval measures and PEASS measures are ``full-reference" measures, i.e., they require the unmixed vocal and/or instrumental tracks to be computed.

\section{Listening Experiment Setup}\label{sec:setup}
In a listening test, five to ten second long excerpts from nine pop music songs were processed using four SVS algorithms.
Thirty subjects were asked to rate the vocals extracted from these excerpts in a series of listening tasks designed as a modified version of the ITU-R BS.1534 MUSHRA standard\cite{vincent2006preliminary,Union2014}, evaluating factors related to the overall quality of extracted vocals, degree of vocal isolation, and preservation of intelligibility\footnote{The data and results from the listening test are available at \url{http://b.gatech.edu/2c1ZLhT}.}.
For further details please refer to the paper by Gupta et.~al.~\cite{gupta2015}.
Te results from the vocal isolation task from this listening test are used to evaluate the performance of the proposed measure and compare it to the performance of established state-of-the-art measures.

It should be noted that the assessment of perceived preservation of intelligibility in vocal extraction using SVS is different than traditional speech intelligibility assessment techniques.
Traditional techniques such as Modified Rhyme Test (MRT) \cite{house1965articulation}, Diagnostic Rhyme Test (DRT) \cite{voiers1977diagnostic}, or Speech Reception Threshold (SRT) \cite{plomp1979improving} are not be applicable in this case as these techniques depend upon the listener having no \textit{a priori} knowledge of the speech content which is a condition that is easily violated in the case of multiple comparisons.
Here, the subjective evaluation is aiming to compare the degree of preservation of perceived intelligibility in the separated vocals when compared to the original as a reference.

\section{Spectral Edge Detection}\label{sec:edge_detection}

\input{fig_stft}

Through feedback from subjects who listened to various samples of extracted vocals \cite{gupta2015},the authors have observed that the most likely cause of loss of intelligibility in the extracted vocal component is due to introduction of spectral artifacts in the process of isolating the vocals. Such artifacts commonly take the form of abrupt glitch like noises in the audio signal, also known as ``musical noise''.
This is due to presence of isolated peaks in the spectrum of the signal and is usually found in outputs of processes involving spectral subtraction or spectral masking\cite{thiemann2001acoustic}.
This kind of processing is very common in SVS algorithms which use these techniques to isolate the contributions of the sources of interest \cite{Emiya2011}.

Figures \ref{fig:stft}(a-c) show examples of the short-time Fourier transform (STFT) spectrogram from the same excerpt for the mixed signal and two extracted vocals using two different systems.
For this excerpt the intelligibility of the first result (Fig.~\ref{fig:JL1STFT}) is rated higher than the second (Fig.~\ref{fig:RNASTFT}).
It is observed that there is an increase in abruptness in the spectral content as intelligibility decreases, i.e., there is a sharper contrast in the spectral content of the less intelligible samples as compared to the more intelligible ones.

These observations are the motivation behind the design of the proposed evaluation measure VIPS.
The following section discusses some proposed features that are an attempt to capture the effects of these observations.

\vspace*{-.5em}
\subsection{Feature Extraction}\label{ssec:extraction}

For the extraction of the features, the extracted vocal signal $\hat{x}_\mathrm{v}$ and the mixed signal $x_\mathrm{m}$ are re-sampled to 16 KHz if the sampling rates are different, and their short-time Fourier transforms  $\hat{X}_\mathrm{v}(n,k)$ and $X_\mathrm{m}(n,k)$ are computed by using thirty-two milliseconds Hamming window with a fifty percent overlap for the n\textsuperscript{th} DFT bin and the k\textsuperscript{th} analysis window. For each STFT, the power spectrogram is calculated as shown in Eq.~\eqref{eq:powerSpec} below.

\begin{equation}\label{eq:powerSpec}
P_\mathrm{j}(n,k) = 20\, \mathrm{log}_{10} |X_\mathrm{j}(n,k)|
\end{equation}

Two Sobel operators $g^\mathrm{x}$ and $g^\mathrm{y}$, as defined below\cite{GonzalezWoods2007,Sobel1968}, are used in order to determine the edges in the power spectrogram.

\vspace*{-.5em}
\begin{align}\label{eq:sobel}
g^\mathrm{x} &:= \frac{1}{8}\begin{bmatrix*}[r] -1 & \phantom{-}0 & \phantom{-}1 \\ -2 & 0 & 2 \\ -1 & 0 & 1 \end{bmatrix*} \refstepcounter{equation}\subeqn \\
g^\mathrm{y} &:= \frac{1}{8}\begin{bmatrix*}[r] -1 & -2 & -1 \\ 0 & 0 & 0 \\ 1 & 2 & 1 \end{bmatrix*} \subeqn
\end{align}

The x and y gradients of the power spectrograms are computed by their two-dimensional convolution with each of the Sobel operators as shown in equations \eqref{eq:conv2xv} to \eqref{eq:conv2ym}.
The gradient $G^\mathrm{x}_\mathrm{v}$ highlights the sharp discontinuities perpendicular to the time axis in the power spectrogram and the gradient $G^\mathrm{y}_\mathrm{v}$ does the same for discontinuities perpendicular to the frequency axis.
This is shown in Figs.~\ref{fig:stft}(d-f) and \ref{fig:stft}(g-i), respectively.

\vspace*{-.5em}
\begin{align}
G^\mathrm{x}_\mathrm{v}(n,k) &=  \hat{P}_\mathrm{v}(n,k) * g^\mathrm{x}\refstepcounter{equation}\subeqn \label{eq:conv2xv}\\
G^\mathrm{x}_\mathrm{m}(n,k) &=  P_\mathrm{m}(n,k) * g^\mathrm{x}\subeqn \label{eq:conv2xm}\\
G^\mathrm{y}_\mathrm{v}(n,k) &=  \hat{P}_\mathrm{v}(n,k) * g^\mathrm{y}\subeqn \label{eq:conv2yv}\\
G^\mathrm{y}_\mathrm{m}(n,k) &=  P_\mathrm{m}(n,k) * g^\mathrm{y}\subeqn \label{eq:conv2ym}
\end{align}

For the purpose of weighting the edges, a weighted loudness FIR filter is designed using recommendation ITU-R BS.468-4\cite{itu1986}. Its magnitude frequency response is defined as $H_\mathrm{w}(f)$ or the discrete version $H_\mathrm{w}[n]$ which is computed for $f$ equals $f_\mathrm{n}$, the central frequency of the n\textsuperscript{th} DFT bin.
This is shown in Fig.~\ref{fig:filter}.
Alternatively, the edges can be weighted uniformly with weights of one. In this case $H_\mathrm{w}[n]$ is one for all values of $n$.

Edges in the spectrum are determined by thresholding the gradients obtained in equations \eqref{eq:conv2xv} to \eqref{eq:conv2ym} in two ways.
First, the threshold values $\mathcal{T}_\mathrm{x}$ and $\mathcal{T}_\mathrm{y}$ are defined and the edge position matrices are calculated for both the separated vocal and the mix signal in x and y directions as shown in Eqs.~\eqref{eq:Ex} and \eqref{eq:Ey}, where $\kappa_\mathrm{x}$ and $\kappa_\mathrm{y}$ are constants between zero and one.
The results in this paper were obtained using $\mathcal{T}_\mathrm{x}$, $\mathcal{T}_\mathrm{y}$, $\kappa_\mathrm{x}$ and, $\kappa_\mathrm{y}$ as 10, 5, 0.8 and, 0.6 respectively, with there values determined experimentally.

\vspace*{-.5em}
\begin{align}
E_\mathrm{x}(n,k)=\begin{cases}
H_\mathrm{w}[n] & \mbox{if} \; \begin{matrix} G^\mathrm{x}_\mathrm{v}(n,k)\geq\mathcal{T}_\mathrm{x} \quad \& \\ G^\mathrm{x}_\mathrm{m}(n,k)<\kappa_\mathrm{x}\mathcal{T}_\mathrm{x} \end{matrix} \\
0 & \mbox{otherwise}
\end{cases}\refstepcounter{equation}\subeqn \label{eq:Ex}\\
E_\mathrm{y}(n,k)=\begin{cases}
H_\mathrm{w}[n] & \mbox{if} \; \begin{matrix} G^\mathrm{y}_\mathrm{v}(n,k)\geq\mathcal{T}_\mathrm{y} \quad \& \\ G^\mathrm{y}_\mathrm{m}(n,k)<\kappa_\mathrm{y}\mathcal{T}_\mathrm{y} \end{matrix} \\
0 & \mbox{otherwise}
\end{cases}\subeqn \label{eq:Ey}
\end{align}

Optionally, for the second step in the thresholding process, simultaneous global masking threshold $M(n,k)$ is calculated along with the loudness estimate $L(n,k)$ (in dbSPL) for each STFT frame for $\hat{x}_\mathrm{v}$ using the MPEG I standard psychoacoustic model specifications\cite{ISO11172}.
The edges $E_\mathrm{x}(n,k)$ or $E_\mathrm{y}(n,k)$ which correspond to a loudness estimate below the global masking threshold are disregarded by equating them to zero.
This is done in order to mitigate the affect of the discontinuities in the gradients which are not perceptually relevant as it can be argued that they will not affect the perceived intelligibility.
Feature extraction was tested both with and without applying the masking threshold and for the results in this paper the analysis without masking threshold provided better results.
However, at the time of publication, it could not be determined if this would hold true with use of more advanced simultaneous and temporal masking models.

The following features are extracted from $E_\mathrm{x}$ and $E_\mathrm{y}$ as averages of the edge position matrices as given in Eqs.~\eqref{eq:avx} and \eqref{eq:avy}, their standard deviation across time as given in Eqs.~\eqref{eq:stx} and \eqref{eq:sty}, and across frequency components, Eqs.~\eqref{eq:sfx} and \eqref{eq:sfy}. Here, $N$ is the number of DFT bins and $K$ is the number of analysis frames.

\begin{align}
\mathrm{AVX} &= \frac{1}{KN}\sum_{k=0}^{K-1}\sum_{n=0}^{N-1}E_\mathrm{x}(n,k) \refstepcounter{equation}\subeqn \label{eq:avx}\\
\mathrm{AVY} &= \frac{1}{KN}\sum_{k=0}^{K-1}\sum_{n=0}^{N-1}E_\mathrm{y}(n,k) \subeqn \label{eq:avy} \\
\mathrm{STX} &= \sqrt{\frac{1}{K-1}\sum_{k=0}^{K-1}\left( \frac{1}{N}\sum_{n=0}^{N-1}E_\mathrm{x}(n,k) -  \mathrm{AVX}\right)^2} \subeqn \label{eq:stx}\\
\mathrm{STY} &= \sqrt{\frac{1}{K-1}\sum_{k=0}^{K-1}\left( \frac{1}{N}\sum_{n=0}^{N-1}E_\mathrm{y}(n,k) -  \mathrm{AVY}\right)^2} \subeqn \label{eq:sty}\\
\mathrm{SFX} &= \sqrt{\frac{1}{N-1}\sum_{n=0}^{N-1}\left( \frac{1}{K}\sum_{k=0}^{K-1}E_\mathrm{x}(n,k) -  \mathrm{AVX}\right)^2} \subeqn \label{eq:sfx}\\
\mathrm{SFY} &= \sqrt{\frac{1}{N-1}\sum_{n=0}^{N-1}\left( \frac{1}{K}\sum_{k=0}^{K-1}E_\mathrm{y}(n,k) -  \mathrm{AVY}\right)^2} \subeqn \label{eq:sfy}
\end{align}
\vspace{-1.5em}

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.95\columnwidth,trim={0 0 0 7.3mm}]{./figs/filterResponse}
%	\input{./figs/filterResponse}
	\caption{The magnitude response of the perceptual loudness weighting filter based on ITU-R BS.468-4}
	\label{fig:filter}
\end{figure}

\section{Proposed Measure: Vocal Intelligibility Preservation Score (VIPS)}\label{sec:vips}

Based on the features described in the Section~\ref{ssec:extraction}, i.e., AVX, AVY, STX, STY, SFX, and SFY we propose a new objective measure for the assessment of preservation of intelligibility of vocals for singing voice separation: the Vocal Intelligibility Preservation Score (VIPS).
VIPS is modeled using regression analysis with these features as the predictor variables.
The subjective ratings for preservation of intelligibility from the listening experiment discussed in Section~\ref{sec:setup} are normalized and then averaged across the participants resulting in mean of zero and a standard deviation of one.
This normalized and averaged result is used as the target variable for the regression model.
Regression between the predictor and the target variables was performed using a bagged ensemble of twenty regression-trees and ten fold cross-validation with the prediction outputs of the trained models designated as VIPS.

The VIPS results (obtained using cross-validation) were compared to the subjective ratings from the listening test using Pearson's and Spearman's correlation coefficients as shown in \tabref{tab:regResult}.
This table shows the average correlation values along with the confidence intervals at ninety-five percent significance levels in the brackets.
The confidence interval is found using a non-parametric estimate of the cumulative distribution function\cite{wahba1983bayesian} and estimating the region which corresponds to the central ninety-five percent region under the probability distribution curve.
It is found that the best result for VIPS is obtained when the edges are perceptually weighted and the auditory-masking threshold is not applied. For this case, the goodness-of-fit analysis for the regression model provides a mean absolute error (MAE) of 0.28 and the coefficient of determination ($\mathrm{R^2}$) of 0.74.


\begin{table}[tb]
   \centering
   \caption{Average Pearson's and Spearman's correlation coefficients for proposed measure (VIPS) v. intelligibility-preservation ratings with 95\% confidence intervals.}
      \begin{tabular}{|c|c|c|c|}
      \hline
      \textbf{Loudness} & \textbf{Masking} & \textbf{Pearson} & \textbf{Spearman} \bigstrut[t]\\
      \textbf{Weighting} & \textbf{Threshold} & \textbf{Correlation} & \textbf{Correlation} \bigstrut[b]\\
      \hline
      \multirow{2}[1]{*}{Perceptual} & \multirow{2}[1]{*}{Yes} & 0.75  & 0.70 \bigstrut[t]\\
            &       & [+.45,+1.00] & [+.31,+1.00] \\
      \multirow{2}[0]{*}{\textit{\textbf{Perceptual}}} & \multirow{2}[0]{*}{\textit{\textbf{No}}} & \textit{\textbf{0.88}} & \textit{\textbf{0.82}} \\
            &       & \textit{\textbf{[+.78,+1.00]}} & \textit{\textbf{[+.58,+1.00]}} \\
      \multirow{2}[0]{*}{Uniform} & \multirow{2}[0]{*}{Yes} & 0.82  & 0.79 \\
            &       & [+.61,+1.00] & [+.51,+1.00] \\
      \multirow{2}[1]{*}{Uniform} & \multirow{2}[1]{*}{No} & 0.86  & 0.77 \\
            &       & [+.69,+1.00] & [+.49,+1.00] \bigstrut[b]\\
      \hline
      \end{tabular}%
   \label{tab:regResult}%
   \vspace*{-.5em}
 \end{table}%

\vspace{-.75em}
\section{Results}\label{sec:result}
This section compares the performance of VIS to the state-of-the-art objective source separation measures.
\tabref{tab:corrcoef} shows the mean and ninety-five percent confidence interval for Pearson's product-moment correlation and Spearman's rank correlation of the BSS Eval and PEASS measures when compared to the ratings from the listening test.
Although average Pearson's correlation of SIR and SAR is high in the absolute sense, they are inconsistent as evidenced by the large confidence intervals indicating that these measures are not reliable in context of estimating the preservation of intelligibility of the lyrics in the separated vocals.
PEASS measures, IPS and APS show higher average correlation magnitudes and smaller confidence intervals than BSS Eval measures making them better suited for this evaluation.

\input{confidenceTable} %\label{tab:corrcoef}

The proposed measure, VIPS, has significantly higher and more consistent absolute correlation to the subjective intelligibility ratings than the BSS Eval measures (compare with \tabref{tab:corrcoef}).
Although there is an increase in performance for VIPS in terms of average correlation and size of the confidence interval as compared to APS (the best performing current state-of-the-art objective measure), the increase is not large enough for a statistically significant comparison between the two without further analysis.
Despite the lack of a conclusive increase in performance, the advantage of VIPS over APS and other state-of-the-art objective measures is that VIPS is a ``no reference'' measure and unlike others, it does not require the unmixed vocal and accompaniment tracks.
This increases the utility of VIPS for application of SVS in real-world recordings or datasets where the unmixed tracks are often not available.

\vspace{-.75em}
\section{Conclusion}\label{sec:conclusion}
This paper introduces the Vocal Intelligibility Preservation Score (VIPS) which serves as a new objective measure for estimating the preservation of intelligibility in the vocals extracted from a musical mixture of vocals and instrumentals.
VIPS has been designed to measure the effects of ``musical noise'' artifacts by detecting the abruptness in the STFT representation of the extracted vocals.
In contrast to current state-of-the-art source separation measures, the computation of VIPS does not require a reference (i.e., unmixed vocal and/or instrumental track) which makes it more suitable for real-world applications.
Additionally, the results in the paper demonstrate that VIPS shows a greater consistency and higher correlation with subjective scores obtained through listening tests compared to current state-of-the-art source separation measures.
VIPS will be a part of a proposed new set of objective measures for perceptually relevant evaluation of singing voice separation. 
The source code for VIPS is available at \url{http://b.gatech.edu/2c29Zz1}.


% For bibtex users:
\bibliographystyle{IEEEbib}
\bibliography{SVS_Library}


\end{document}
