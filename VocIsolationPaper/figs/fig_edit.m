Ch = get(gca,'Children');
delete(Ch(1:end-6))
clabel([],ctr,'manual','FontSize',12,'FontName','Arial')
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
saveas(gcf,'classifier.pdf')
saveas(gcf,'classifier.png')
saveas(gcf,'classifier.eps')
