% -----------------------------------------------
% Template for ISMIR Papers
% 2016 version, based on previous ISMIR templates

% Requirements :
% * 6+1 page length maximum
% * 2MB maximum file size
% * Copyright note must appear in the bottom left corner of first page
% (see conference website for additional details)
% -----------------------------------------------

% Template for ICASSP-2017 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------

\documentclass{article}
\usepackage{spconf,amsmath,mathtools}
\usepackage{graphicx}
\usepackage{color}
\usepackage{url,times,enumerate,enumitem,float}
\urlstyle{same}
\usepackage{multirow,bigstrut,rotating,booktabs,subcaption}
%\usepackage[font=normal]{subfig}
\usepackage{longtable,ltcaption}
\usepackage{etoolbox}
\usepackage{pgfplots}
\AtBeginEnvironment{align}{\setcounter{subeqn}{0}}% Reset subequation number at start of align
\newcounter{subeqn} \renewcommand{\thesubeqn}{\theequation\alph{subeqn}}%
\newcommand{\subeqn}{%
	\refstepcounter{subeqn}% Step subequation number
	\tag{\thesubeqn}% Label equation
}
\DeclareCaptionJustification{nohyphen}{\hyphenpenalty=10000}
\captionsetup{justification=nohyphen}
%\captionsetup{justification=left}
\hyphenpenalty=800
\widowpenalty=800
\clubpenalty=800
\setlength{\emergencystretch}{2pt}
\usepackage{grfext}
\PrependGraphicsExtensions*{.pdf,.PDF}
\usepackage{cite}
\allowdisplaybreaks
\apptocmd{\thebibliography}{\ninept}{}{}

%\bibliographystyle{unsrt}

% Title.
% ------
\title{On Perceptually Motivated Objective Assessment of Isolation of Vocals for Singing Voice Separation}

% Note: Please do NOT use \thanks or a \footnote in any of the author markup

\name{Udit Gupta$^{\star}$ \qquad
	  Alexander Lerch$^{\dagger}$ \qquad
	  Elliot Moore$^{\star}$
	 } 
\address{$^{\star}$School of Electrical and Comp. Engineering,
		 $^{\dagger}$Center for Music Technology\\
		 Georgia Institute of Technology, Atlanta, GA, USA\\
		 \{uditgupta, alexander.lerch, em80\}@gatech.edu
		}

%% To make customize author list in Creative Common license, uncomment and customize the next line
%  \def\authorname{First Author, Second Author} 

% Three addresses
% --------------
%\threeauthors
%  {First Author} {Affiliation1 \\ {\tt author1@ismir.edu}}
%  {Second Author} {\bf Retain these fake authors in\\\bf submission to preserve the formatting}
%  {Third Author} {Affiliation3 \\ {\tt author3@ismir.edu}}

%% To make customize author list in Creative Common license, uncomment and customize the next line
%  \def\authorname{First Author, Second Author, Third Author} 


\sloppy % please retain sloppy command for improved formatting

\begin{document}
%\ninept
\maketitle

\begin{abstract}
Singing Voice Separation (SVS) uses audio source separation methods to isolate the vocal component from the background accompaniment in a song mix.  Assessing the quality of vocal isolation is an essential element of SVS evaluation.
However, current state-of-the-art objective measures for SVS performance evaluation have not shown a high correlation consistency with subjective assessments.
Additionally, the current state-of-the-art evaluation measures require the use of a reference representing the unmixed vocal and instrumental tracks which are often not available.
This article proposes a new objective measure, Vocal Isolation Score (VIS), which can be computed without a reference track and is intended to improve on the current state-of-the-art in vocal isolation assessment.
Results from a listening test across several SVS methods show that the VIS maintains a higher and more consistent correlation with subjective assessments over current state-of-the-art objective measures. 

\end{abstract}

\begin{keywords}
	Source Separation, Music Information Retrieval, Perceptual Evaluation
\end{keywords}


\section{Introduction}\label{sec:introduction}

Singing Voice Separation (SVS) has gained prominence as a Music Information Retrieval task in recent years;
the goal is to separate the lead vocals from the accompaniment in mixes of (popular) music \cite{Ikemiya2014,Jeong2014,Rafii2013,Rao2014}.
In Music Information Retrieval Evaluation eXchange (MIREX) in recent years \cite{svsTask14,svsTask15,svsTask16}, the performance of various submissions for the SVS task was evaluated using (Normalized) Signal to Distortion Ratio (NSDR),
Signal to Interference Ratio (SIR), and Signal to Artifacts Ratio (SAR) \cite{Vincent2006,Emiya2010}.
However these measures do not correlate well with perceptual results obtained from human evaluation based on listening tests\cite{gupta2015,Emiya2011}.

These measures and other more recent ones \cite{Emiya2011}, need the presence of unmixed vocal and instrumental tracks for evaluation.
Outside of academic datasets, the unmixed tracks are rarely available, limiting the utility of these measures.
This paper is an attempt to address these shortcomings of the current state-of-the-art measures by proposing a 
new objective measure, Vocal Isolation Score (VIS), to better evaluate the performance of various SVS implementations with a closer correlation to the human listening experience in context of isolation of the target in the separated vocals.

VIS has been designed such that it can either be calculated using a ``full-reference'' method, i.e. requiring the use of unmixed vocal and/or instrumental tracks, or  using a ``no-reference'' method, i.e. it does not need the unmixed vocal and instrumental tracks for the evaluation.
As will be demonstrated in the paper, the performance using both the methods was tested and found to be comparable with one another.
This proposal for a new feature is a part of a larger effort which aims to design perceptually related objective measures specifically designed for evaluation of SVS in terms of isolation of vocals, preservation intelligibility, additive and subtractive distortions, as well as the overall perceptual quality.

%The rest of the paper is organized as follows. First, an overview of existing state of the art for the evaluation of SVS implementations is presented in Sect.~\ref{sec:overview}. This includes but is not limited to NSDR, SIR and SAR. 
%In Sect.~\ref{sec:setup}, a small summary of a listening test from an earlier work is provided.
%The ratings obtained from this listening test are correlated with the existing measures and the results obtained are discussed.
%These ratings are also used further in the paper to justify the validity of the proposed features.
%Section \ref{sec:edge_detection} describes the new features, and provides the results of their evaluation.
%The concluding remarks are addressed in Sect.~\ref{sec:conclusion}.

\vspace{-.75em}
\section{Related Work}\label{sec:overview}
There are many examples where perceptual evaluation has been performed
for comparing general audio source separation systems~\cite{Joseph2004,Kornycky2008,Yilmaz2004}. 
Many of these methods are geared towards evaluating source separation in speech-only mixtures and do not transfer
elegantly to SVS evaluation where vocals are mixed with instrumental accompaniment.
Additionally, listening tests are time-consuming, have to be carefully planned,
and are usually restricted to a relatively small subset of audio files.
To address these challenges some objective methods for performance evaluation have been suggested.

Emiya et al.\cite{Vincent2006} have suggested objective measures based on the presence of 
target spatial distortion (Image to Spatial Distortion Ratio, ISR), 
interference (SIR), and artifacts (SAR) in the separated signals as compared to the clean source signals.
The total distortion in the output signal compared to the source is measured by Signal to Distortion Ratio (SDR)\cite{Araki2012}.
These measures model the test signal (extracted vocal) as a linear mixture of the target signal (true vocal), interfering signals (true instrumental), and noise (processing artifacts). The contribution of each of these sources is estimated to get the values for SIR, SAR and (N)SDR. These measures are a part of the publicly available BSS Eval Toolbox\cite{Vincent2006} and will be collectively referred to as BSS Eval measures in this paper.
SIR and SAR were used to evaluate the submissions in the MIREX Singing Voice Separation tasks, along with the normalized version of SDR designated as NSDR\cite{svsTask14}. Moderate correlation in the range of 0.3 to 0.7 has been reported for these measures when compared to judgment by human evaluators for general audio source separation tasks\cite{Emiya2011,fox2007modeling}.

Another set of measures have been developed as improved versions of BSS Eval measures.
These measures, referred to as OPS (Overall Perceptual Score), TPS (Target-related Perceptual Score), IPS (Interference-related Perceptual Score), and APS (Artifacts-reated Perceptual Score) are designed by taking the human loudness perception models into account and have been reported to have higher correlation scores as compared to BSS Eval measures \cite{Emiya2011}.
OPS, TPS, IPS and APS are available as parts of PEASS Toolbox\cite{Emiya2011,vincent2012improved} and will be referred to as PEASS measures collectively in this paper.
Both BSS Eval measures and PEASS measures are ``full-reference" measures, i.e., they require the unmixed vocal and/or instrumental tracks to be computed.

\vspace{-.75em}
\section{Listening Experiment Setup}\label{sec:setup}
In a listening test, five to ten second long excerpts from nine pop music songs were processed using four SVS algorithms.
Thirty subjects were asked to rate the vocals extracted from these excerpts in a series of listening tasks designed as a modified version of the ITU-R BS.1534 MUSHRA standard\cite{vincent2006preliminary,Union2014}, evaluating factors related to the overall quality of extracted vocals, degree of vocal isolation, and preservation of intelligibility\footnote{The data and results from the listening test are available at \url{http://b.gatech.edu/2c1ZLhT}.}.
For further details please refer to the paper by Gupta et.~al.~\cite{gupta2015}.

Te results from the vocal isolation task from this listening test are used to evaluate the performance of the proposed measure and compare it to the performance of established state-of-the-art measures.

\vspace{-.75em}
\section{Vocal Isolation Score (VIS)}\label{sec:VIS}

Vocal Isolation Score is a new measure proposed to evaluate the performance of SVS algorithms in context of isolation of the vocals.
The separation process for extraction of the vocals is not perfect and often results in the presence of interference in the form of residual instrumentals.
VIS is designed to correlate with the loudness of this residual instrumental signal such that extracted vocals with lesser amount of residual instrumentals score better than ones which have more residuals and hence have poor vocal isolation.
This measure is derived using regression from a set of features which are sensitive to the relative loudness levels of instrumentals and the vocals.

\subsection{Feature Extraction}\label{ssec:MFCCfeatureExtraction}

Lehner et al. \cite{lehner2013towards} demonstrated that appropriately parameterized Mel-Frequency Cepstral Coefficients (MFCC) perform comparably well to other more complex set of features in their paper about detecting regions where singing voice is present in music.
The authors independently verified their results through within and cross-dataset testing using excerpts from MedleyDB Multitrack Dataset \cite{bittner2014medleydb} and iKala Dataset \cite{Chan2015Ikala}.
The results from this work were the motivation behind investigating the perceptual significance of MFCC based features for assessment of vocal isolation from musical mixes as described in \secref{ssec:feateval}.

The feature extraction process begins with the normalization of extracted vocals with respect to loudness.
The loudness of the extracted vocals is estimated using Zwicker Loudness Estimate (ISO 532B) \cite{moore1996revision,zwicker1991program} and the signal is scaled such that the loudness of the normalized audio is eighty phons.
The normalized audio signal is then divided into frames of duration eight hundred milliseconds using a Hann window with seventy-five percent overlap between adjacent frames.
MFCCs are calculated for each audio frame using thirty spectral bands between zero and the Nyquist frequency.
The first thirty MFCCs are retained for each frame and $\mathrm{{\Delta}MFCCs}$ are calculated by finding the difference between the coefficients for adjacent frames.

The frames without any vocal activity are identified and removed so as not to bias the features with periods of silence or instrumental only sections.
This can be achieved by using either the ``full-reference'' method or the ``no-reference'' method.
The full-reference method uses the unmixed vocals track to detect frames with presence of acoustic activity by means of thresholding the energy present in each of the analysis-frames.
This serves as a crude but effective acoustic activity detector which allows for the frames without any vocal activity to be removed.
The no-reference method uses a classifier designed to detect regions without vocal activity using the method described by Lehner et al. \cite{lehner2013towards} and operates on the original musical mix.
Unlike Lehner et al. \cite{lehner2013towards} who use a support vector machine based classifier, the results reported in this paper were computed using an implementation of a random forest classifier with 201 trees, which was trained using one-hundred songs from a mixed set of samples from MedleyDB Multitrack Dataset \cite{bittner2014medleydb} and iKala Dataset \cite{Chan2015Ikala}.
As excerpts from MedleyDB Dataset are also used as a part of the listening test described in \secref{sec:setup}, it was ensured that the two sets did not overlap in song and artist choices.
The validation statistics of fifty iterations of randomly sampled songs for training vs.~testing sets (with twenty-five percent holdout) are shown in \figref{fig:classifier}.
The figure shows a scatter of precision and recall for all the fifty samples with the average and standard deviation denoted by the cross.
The ninety-five percent confidence bound (assuming multivariate normal distribution), shown as the ellipse, indicates that the worst case performance of the classifier results in precision values of about seventy-three percent.
The classifier operating point is cost biased to assign twice the importance for precision than recall because in our case, the number of vocal frames correctly identified matter less than the accuracy of the identified vocal frames.

\begin{figure}[t]
\centering
\includegraphics[width=0.98\linewidth,trim={0 0 0 10mm},clip]{figs/classifierIS}
\caption{Precision v. Recall for fifty iterations of non-vocal activity classifier. The cross shows the mean and standard deviation, ellipse is the 95\% confidence bound and contours are the F1-statistic.}
\label{fig:classifier}
\vspace{-1.0em}
\end{figure}

After the non-vocal frames have been removed, the means and standard deviations of the first thirty MFCCs and $\mathrm{{\Delta}MFCCs}$ (including zero order MFCC) are found.
This results in a feature vector of one-hundred and twenty dimensions for each of the extracted vocal excerpts.
These features will be referred to as $\mathrm{MFCC}_\mu^k$, $\mathrm{MFCC}_\sigma^k$, $\Delta \mathrm{MFCC}_\mu^k$ and, $\Delta \mathrm{MFCC}_\sigma^k$ (where $k$ is the order of the coefficient from 0 to 29) in the remainder of the paper.

Upon comparisons using paired T-tests it was determined that there was no statistically significant difference between the features from the ``full-reference'' and the ``no-reference'' methods.

\subsection{Feature Sensitivity to Instrumental Mixing Levels}\label{ssec:feateval}

To be useful for VIS, it is important that the above features are sensitive to the level of instrumental presence in the extracted vocals.
To test this, audio clips, from twenty five songs, were mixed with different levels of instrumentals as compared to vocals at +5 dB, 0 dB, and -5 dB.
These clips were normalized such that the overall loudness of each of the clips was the same.
The MFCC based features extracted from all these clips along with the vocals only clips ($-\mathrm{\infty}$ dB) and the instrumentals only clip ($+\mathrm{\infty}$ dB) were compared using pair-wise Kolmogorov-Smirnov tests between the feature-sets ordered by increasing instrumental loudness that are shown in \tabref{tab:pvalue}.
The comparisons for which the null hypothesis (that the two sets of samples belong to identical distributions) can be rejected with $\alpha=0.05$ are marked with ``x''.

For means of MFCCs and  $\mathrm{\Delta}$MFCCs, the feature distributions are statistically different only with respect to absence or presence of vocals.
This can be observed from \tabref{tab:pvalue}; differences for MFCCs and  $\mathrm{\Delta}$MFCCs are not significant when comparing between clips with different levels of instrumentals, and only show significant differences between the feature distributions when comparison is between the vocals-only clips and clips with instrumentals mixed in.
On the other hand, the standard deviations of MFCCs and  $\mathrm{\Delta}$MFCCs show statistically significant variations with different levels of instrumentals present in the audio clips, and therefore are sensitive to the relative loudness of the instrumentals present in the clips.
This analysis justifies the use of these features for VIS because, they are sensitive to the amount of instrumental signal present in the extracted vocals as residual interference.

\begin{table}[tb]
   \centering
   \ninept
   \setlength{\tabcolsep}{3.5pt}
   \caption{Results from paired Kolmogorov-Smirnov test comparing the four categories of features for decreasing level of relative instrumental loudness. Statistically significant values with $\alpha=0.05$ marked as x.}
     \begin{tabular}{lcccc}
          \toprule
          Instr. Levels & \multicolumn{1}{l}{$\mathrm{MFCC_{\mu}}$} & \multicolumn{1}{l}{$\mathrm{MFCC_{\sigma}}$} & \multicolumn{1}{l}{$\mathrm{{\Delta}MFCC_{\mu}}$} & \multicolumn{1}{l}{$\mathrm{{\Delta}MFCC_{\sigma}}$} \\
          \midrule
          $+\mathrm{\infty}$ dB v. +5 dB & -    & x    & -    & x \\
          +5 dB v. 0 dB & -    & x    & -    & x \\
          0 dB v. -5 dB & -    & x    & -    & x \\
          -5 dB v. $-\mathrm{\infty}$ dB & x    & x    & x    & x \\
          \bottomrule
          \end{tabular}%
   \label{tab:pvalue}%
   \vspace{-.75em}
 \end{table}%
 

\subsection{Dimensionality Reduction and Regression}\label{ssec:regression}

The feature set of one-hundred and twenty dimensions shows that it has a lot of redundant features, with high inter-feature covariance.
This requires the feature set to be reduced to a smaller dimensionality while removing the redundant information.
For this purpose, principal component analysis (PCA) was performed on each set of features $\mathrm{MFCC}_\mu^k$, $\mathrm{MFCC}_\sigma^k$, $\Delta \mathrm{MFCC}_\mu^k$ and, $\Delta \mathrm{MFCC}_\sigma^k$; and from each of the PCA rotated feature-sets, only the features that explained at least five percent of the variability in the data were retained.
This process resulted in a set of feature-vectors with a reduced dimensionality equal to thirteen that are combined using regression.

The subjective ratings for isolation of vocals from the listening experiment discussed in Section~\ref{sec:setup} are normalized and then averaged across the participants resulting in mean of zero and a standard deviation of one.
This normalized and averaged result is used as the target variable for the regression model.
VIS is calculated as the result of regression between the predictor and the target variables using a bagged ensemble of fifty regression-trees and ten fold cross-validation.

The VIS results (obtained using cross-validation) were compared to the subjective ratings from the listening test using Pearson's and Spearman's correlation coefficients as shown in \tabref{tab:VIScorrelation}.
This table shows the average correlation values along with the confidence intervals at ninety-five percent significance levels in the brackets.
The confidence interval is found using a non-parametric estimate of the cumulative distribution function\cite{wahba1983bayesian} and estimating the region which corresponds to the central ninety-five percent region under the probability distribution curve.
It is found that the results for VIS calculated using full-reference method and no-reference method are statistically similar to each other, with the no-reference measure performing slightly better in terms of Spearman's rank correlation as compared to VIS calculated using full-reference method.

\begin{table}[tb]%\label{tab:VIScorrelation}
  \centering
  \ninept
    \caption[Correlation of Vocal Isolation Score v. perceptual vocal isolation]{Average Pearson's and Spearman's correlation coefficients for Vocal Isolation Score (VIS) v. perceptual vocal isolation ratings with 95\% confidence intervals}
   \begin{tabular}{ccc}
       \toprule
       \textbf{Feature} & \textbf{Pearson} & \textbf{Spearman} \\
       \textbf{Type} & \textbf{Correlation} & \textbf{Correlation} \\
       \midrule
       \multirow{2}[1]{*}{VIS: Full-Reference} & 0.73 & 0.67 \\
            & [+.42,+1.00] & [+.18,+1.00] \\
       \multirow{2}[1]{*}{VIS: No-Reference} & 0.74 & 0.70 \\
            & [+.43,+1.00] & [+.28,+1.00] \\
       \bottomrule
   \end{tabular}%
  \label{tab:VIScorrelation}
  \vspace{-.75em}
\end{table}%


\vspace{-.5em}
\section{Results}
This section compares the performance of VIS to the state-of-the-art objective source separation measures.
\tabref{tab:corrcoef} shows the mean and ninety-five percent confidence interval for Pearson's product-moment correlation and Spearman's rank correlation of the BSS Eval and PEASS measures when compared to the subjective evaluation of isolation of vocals.
Although both average Pearson's and Spearman's correlations of SIR of BSS Eval measures are high in the absolute sense, they are inconsistent as evidenced by the large confidence intervals.
Additionally, Pearson's correlation for SIR has confidence intervals which exists both in positive and negative regions indicating that SIR inconsistently switches between increasing and decreasing values when comparing SVS evaluations with increasing perceptual scores for vocal isolation.
Similarly among the PEASS measures, IPS shows the highest average Pearson's correlation magnitude but its performance falls below SIR when comparing their Spearman's correlation statistics.
Like SIR, IPS also suffers from inconsistency issues with large confidence intervals spanning both positive and negative values.

The proposed measure, VIS (refer \tabref{tab:VIScorrelation}), has significantly higher and more consistent correlation to the subjective vocal isolation ratings than the BSS Eval or PEASS measures.
VIS has higher average correlation with subjective vocal isolation ratings than either SIR or IPS, the best performing state-of-the-art measures in their categories, and unlike SIR and IPS, the correlation results for VIS do not span both positive and negative values as evidenced by the confidence interval.

The results presented here demonstrate conclusively that the proposed measure VIS performs substantially better than the current state-of-the-art source separation evaluation measures for evaluating the isolation of vocals in SVS processed audio.
A very big advantage of VIS over SIR, IPS, and other state-of-the-art objective measures is that VIS can be calculated as a ``no reference'' measure and unlike others, it does not require the unmixed vocal and accompaniment tracks.
This increases the utility of VIS for application of SVS in real-world recordings or datasets where the unmixed tracks are often not available.


\begin{table}[tb] %\label{tab:soa-isolation}
\centering
\ninept
\caption[Performance of state of the art features for vocal isolation]{Average Pearson's and Spearman's correlation coefficients for established state-of-the-art source separation measures v. perceptual vocal isolation ratings with 95\% confidence intervals}
\centering
	% Table generated by Excel2LaTeX from sheet 'Sheet1'
	\begin{tabular}{|r|c|c|c|}
	\cline{3-4}\multicolumn{1}{r}{} &   & \textbf{Pearson} & \textbf{Spearman} \bigstrut\\
	\hline
	\multirow{6}[2]{*}{\begin{sideways}\textbf{BSS Eval}\end{sideways}} & \multirow{2}[1]{*}{\textbf{NSDR}} & 0.122 & 0.409 \bigstrut[t]\\
	  &   & [-0.95,+0.97] & [-0.53,+0.78] \\
	  & \multirow{2}[0]{*}{\textbf{SIR}} & 0.567 & 0.701 \\
	  &   & [-0.65,+1.00] & [-0.05,+0.88] \\
	  & \multirow{2}[1]{*}{\textbf{SAR}} & -0.332 & 0.069 \\
	  &   & [-1.00,+0.82] & [-0.72,+0.61] \bigstrut[b]\\
	\hline
	\multirow{8}[2]{*}{\begin{sideways}\textbf{PEASS}\end{sideways}} & \multirow{2}[1]{*}{\textbf{OPS}} & 0.102 & 0.070 \bigstrut[t]\\
	  &   & [-0.79,+0.94] & [-0.90,+0.94] \\
	  & \multirow{2}[0]{*}{\textbf{TPS}} & -0.554 & -0.511 \\
	  &   & [-1.00,+0.38] & [-1.00,+0.44] \\
	  & \multirow{2}[0]{*}{\textbf{IPS}} & 0.638 & 0.638 \\
	  &   & [-0.38,+1.00] & [-0.28,+1.00] \\
	  & \multirow{2}[1]{*}{\textbf{APS}} & -0.488 & -0.398 \\
	  &   & [-1.00,+0.60] & [-0.99,+0.61] \bigstrut[b]\\
	\hline
	\end{tabular}%
\label{tab:corrcoef}%
\end{table}%


\section{Conclusion}\label{sec:conclusion}
This paper introduces the Vocal Isolation Score (VIS) which serves as a new objective measure for estimating vocal isolation in the vocals extracted from a musical mixture of vocals and instrumentals.
In contrast to current state-of-the-art source separation measures, the computation of VIS does not require a reference (i.e., unmixed vocal and/or instrumental track) which makes it more suitable for real-world applications.
Additionally, the results in the paper demonstrate that VIS shows a greater consistency and higher correlation with subjective scores obtained through listening tests compared to current state-of-the-art source separation measures.
VIS will be a part of a proposed new set of objective measures for perceptually relevant evaluation of singing voice separation. 
The source code for VIS is available at \url{http://b.gatech.edu/2c29Zz1}.



% For bibtex users:
\bibliographystyle{IEEEbib}

\bibliography{SVS_Library}


\end{document}
